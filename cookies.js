var Cookies = (function() {
	var cookieManager = {};

	const EU_COOKIES_DIRECTIVE = "cookieconsent_dismissed";

	cookieManager.acceptUEDirective = function() {
		setCookie(EU_COOKIES_DIRECTIVE, "yes");
	};

	cookieManager.hasAcceptedUEDirective = function() {
		var directive = getCookie(EU_COOKIES_DIRECTIVE);
		return directive != null && directive === "yes";
	};

	cookieManager.get = function(name) {
		checkUEDirective();
		return getCookie(name);
	};

	cookieManager.set = function(name, value, days) {
		checkUEDirective();
		setCookie(name, value, days);
	};

	function checkUEDirective() {
		if (cookieManager.hasAcceptedUEDirective())
			return;

		console.error("Cookies UE Directive not accepted yet");
		throw "Cookies UE Directive not accepted yet";
	}

	function getCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	};

	function setCookie(name,value,days) {
		var expires = "";
		if (days) {
			var date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			expires = "; expires="+date.toGMTString();
		}
		document.cookie = name+"="+value+expires+"; path=/";
	};

	return cookieManager;
})();